package ExceptionHandling;

import java.util.InputMismatchException;
import java.util.Scanner;



public class ExceptionExample {

	static int a,b,c;
	
	
	static int takeIntValue()
	{
		Scanner sc = new Scanner(System.in);
		try {
			return sc.nextInt();
			}catch(InputMismatchException i)
			{
				System.out.println("Enter numeric value");
				sc.nextLine();
				return sc.nextInt();
			}
	}
	
	public static void main(String[] args) {
		
		/*System.out.println("Line 1");
		int m = 10/0;
		System.out.println("Line 2");
		System.out.println("Line 3");*/
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first no:");
		 
		a= ExceptionExample.takeIntValue();
		System.out.println("Enter second no:");
		
		b= ExceptionExample.takeIntValue();
		
		 
		try {
		c = a/b;
		}catch(ArithmeticException e)
		{
			//System.out.println(e.getMessage());
			e.printStackTrace();
			System.out.println("Enter second no which is non zero:");
			b = sc.nextInt();
			c= a/b;
		}
		System.out.println("Value of divison : "+c);
		
		
		
	}
}
