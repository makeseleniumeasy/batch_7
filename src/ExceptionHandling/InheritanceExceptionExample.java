package ExceptionHandling;

import java.io.FileNotFoundException;

class SuperCLass
{
	void add () throws FileNotFoundException
	{
		System.out.println("Super class");
		int b = 12/0;
	}
}
public class InheritanceExceptionExample extends SuperCLass{
	
	void add () throws ArithmeticException
	{
		System.out.println("SUb class");
		int b = 12/0;
	}

	
	void print() throws FileNotFoundException
	{
		
	}
	
	public static void main(String[] args) {
		
		/*EnheritanceException e = new EnheritanceException();
		e.add();*/
		
		
		SuperCLass dsfdskfh = new SuperCLass();
		dsfdskfh.add();
	}
}
