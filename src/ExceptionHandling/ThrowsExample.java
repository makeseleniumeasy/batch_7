package ExceptionHandling;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ThrowsExample {
	
	
	public static void readFile() throws FileNotFoundException, InterruptedException, Exception
	{
		try {
		FileInputStream fis= new FileInputStream(new File("c:/demo.txt"));
		}catch(ArithmeticException e)
		{
			System.out.println("File not found 2");
		}
	}
	
	
	void test() throws FileNotFoundException, ArithmeticException 
	{
		
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		
		try {
		readFile();
		}catch(Exception e)
		{
			System.out.println("File not found");
		}
	}

}
