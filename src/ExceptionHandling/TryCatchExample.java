package ExceptionHandling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TryCatchExample {
	
	
	static Scanner sc= new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.println("Program starts");
		try {
		System.out.println("Enter first no:");
		int a = sc.nextInt();
		int b = 10/a;
		}catch(NullPointerException n)
		{
			System.out.println("SC is null.");
		}
		catch(InputMismatchException i)
		{
			System.out.println("Input mismatch");
		}
		
		System.out.println("Program ends");
	}

}
