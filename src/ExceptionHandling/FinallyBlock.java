package ExceptionHandling;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class FinallyBlock {
	
	public static void main(String[] args) throws FileNotFoundException{
		
		//int c= 10/0;
		
		try {
			int c= 10/0;
			FileInputStream fis= new FileInputStream(new File("C:\\Users\\ROHINI\\Desktop/demo.txt"));
		}
		/*catch(FileNotFoundException f)
		{
			System.out.println("File not found");
		}*/
		finally
		{
			System.out.println("Clean up code");
		}
	}

}
