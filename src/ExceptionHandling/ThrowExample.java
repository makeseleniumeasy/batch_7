package ExceptionHandling;

import java.io.FileNotFoundException;

public class ThrowExample {
	
	public static void add() throws FileNotFoundException
	{
		int a = 10;
		int b =0;
		if(b==0)
			throw new FileNotFoundException();
	}
	
	public static void main(String[] args) {
		
		try {
		add();
		}catch(Exception e)
		{
			System.out.println("B is zero");
		}
	}

}
