package WrapperClass;

public class ComparingWrapperClassObject {
	
	int i = 10;
	
	public static void main(String[] args) {
		
		/*ComparingWrapperClassObject c1 = new ComparingWrapperClassObject();
		ComparingWrapperClassObject c2 = new ComparingWrapperClassObject();
		
		
		System.out.println(c1==c2);  
		
		System.out.println(c1.equals(c2));*/
		
		
		/*Integer i1 = 10;
		Integer i2 = 10;
		
		System.out.println(i1==i2);
		System.out.println(i1.equals(i2));
		
		
		Integer i3 = 128;
		Integer i4 = 128;
		
		System.out.println(i3==i4);
		System.out.println(i3.equals(i4));*/
		
		
		Integer i1 = new Integer(10);
		Integer i2 = new Integer(10);
		
		System.out.println(i1==i2);
		System.out.println(i1.equals(i2));
		
		
		Integer i3 = new Integer(128);
		Integer i4 = new Integer(128);
		
		System.out.println(i3==i4);
		System.out.println(i3.equals(i4));
	}

}
