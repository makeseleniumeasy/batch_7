package WrapperClass;

public class IntegerClass extends Object{
	
	
	public static void main(String[] args) {
		
		Integer i1 = new Integer(10);
		System.out.println(i1);
		System.out.println(i1.toString());
		
		
		/*IntegerClass i = new IntegerClass();
		System.out.println(i);*/
		
		Integer i2 = 20;
		System.out.println(i2);
		
		
		int i3= 40;
		Integer i4 = i3;
		System.out.println(i4);
		
		
		
		Byte b1 = 10;
		System.out.println(b1);
		
		//Byte b2 = 130;
		
		Integer i5 = i1+i2;
		System.out.println(i5);
		
		
		int i6 = i5;
		System.out.println(i6);
		
		
	}

}
