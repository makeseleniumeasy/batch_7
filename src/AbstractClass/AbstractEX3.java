package AbstractClass;

public abstract class AbstractEX3 {
	
	//private abstract void   disp();
	
	// final abstract void   disp();
	
	// static abstract void disp();
	
	
	int i = 10;
	
	
	AbstractEX3()
	{
		System.out.println("COnstructir");
	}
	

}


class CompleteAbstractEX3 extends AbstractEX3
{
	CompleteAbstractEX3()
	{
		System.out.println("Child constructor");
	}
	
	
	public static void main(String[] args) {
		
		CompleteAbstractEX3 s = new CompleteAbstractEX3();
		System.out.println(s.i);
	}
}
