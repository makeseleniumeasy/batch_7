package AbstractClass;

abstract class AbstractEx1{
	
	public abstract void add();
	public abstract void substract();
	public abstract void division();
	public abstract void multiplication();
	
	
	public /*abstract*/ void print()
	{
		System.out.println("Print");
	}
		

}



public class AbstractComplete extends AbstractEx1
{

	@Override
	public void add() {
		System.out.println("Addition");
		
	}

	@Override
	public void substract() {
		System.out.println("Sub");
		
	}

	@Override
	public void division() {
		System.out.println("Div");
		
	}

	@Override
	public void multiplication() {
		System.out.println("Mul");
		
	}
	
	
	public void show()
	{
		System.out.println("Show");
	}
	
	public static void main(String[] args) {
		
		//AbstractEx1 abs = new AbstractEx1();
		/*AbstractComplete abs = new AbstractComplete();
		abs.add();
		abs.substract();
		abs.division();
		abs.multiplication();*/
		
		
		AbstractComplete abs = new AbstractComplete();
		abs.add();
		abs.substract();
		abs.division();
		abs.multiplication();
		abs.print();
		//abs.show();
		
	}
	
}


