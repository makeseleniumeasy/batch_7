package AbstractClass;


abstract class A
{
	abstract void add();
}


abstract class B
{
	abstract void add();
}

public class MultipleInheritanceAbstractCLass extends A, B{
	
	void add()
	{
		
	}
	
	
	/*void add()
	{
		
	}*/
	

}
