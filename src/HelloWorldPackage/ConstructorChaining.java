package HelloWorldPackage;

public class ConstructorChaining {
	
	int a;
	int b;
	int c;
	
	ConstructorChaining(int a1, int b1, int c1)
	{
		this(b1,c1);
		a=a1;
		System.out.println("first constructor");
	}
	
	ConstructorChaining(int b1, int c1)
	{
		this(c1);
		b= b1;
		System.out.println("second constructor");
	}
	
	
	ConstructorChaining(int c1)
	{
		c= c1;
		System.out.println("third constructor");
	}
	
	
	public static void main(String[] args) {
		
		ConstructorChaining obj = new ConstructorChaining(100, 200, 300);
		System.out.println(obj.a);
		System.out.println(obj.b);
		System.out.println(obj.c);
	}

}
