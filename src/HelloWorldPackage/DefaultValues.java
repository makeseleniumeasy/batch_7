package HelloWorldPackage;

public class DefaultValues {
	
	//int i = 10;
	
	int i;
	
	float f;
	
	byte b;
	short s;
	char c;
	String st;
	long l;
	double d;
	boolean bool;
	
	
	
	public static void main(String[] args) {
		
		DefaultValues d = new DefaultValues();
		System.out.println("int :"+d.i);
		System.out.println("float "+d.f);
		System.out.println("byte "+d.b);
		System.out.println("short "+d.s);
		System.out.println("char "+d.c);
		System.out.println("String "+d.st);
		System.out.println("long "+d.l);
		System.out.println("double "+d.d);
		System.out.println("boolean "+d.bool);
	}

}
