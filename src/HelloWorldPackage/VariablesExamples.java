package HelloWorldPackage;

public class VariablesExamples {
	
	/*
	 * byte( 1 byte) , short( 2 byte), int( 4 byte) , long( 8 byte), float( 4 byte) , double( 8 byte), boolean , char, String
	 */
	
	
	public static void main(String[] args) {
		
		/*
		 * To store whole numbers.
		 */
		
		byte b1 = 121;
		System.out.println(b1);
		
		
		short s1 = 130;
		System.out.println(s1);
		
		int i1 = 19090;
		System.out.println(i1);
		
		
		long l1 =  822337203685477580l;
		System.out.println(l1);
		
		
		
		// Decimal numbers
		
		float f1 = 123.457812321212f;
		System.out.println(f1);
		
		double d1= 343.455465465768686876876868;
		System.out.println(d1);
		
		// Arithmetic operations
		
		byte b2= 120;
		byte b3=120;
		
		byte b4 = (byte) (b2+b3);
		
		
		System.out.println(b4);
		
		
		boolean boolean1= true;
		boolean boolean2= true;
		
		//boolean boolean3= boolean1+boolean2;
		
		
		char c1 = 'அ';
		System.out.println(c1);
		
		String str1 = "f";
		System.out.println(str1);
		
		
		
	}

	
	
}
