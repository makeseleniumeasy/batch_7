package MethodsExasmples;

public class MethodSignature {
	
	/*
	 * 
	 * SIgnature of method:
	 * 1. Name of method
	 * 2. No of arguments
	 * 3. Order of arguments
	 * 4. TYpe of arguments
	 * 
	 * 
	 * Method overloading
	 */
	
	void m1()
	{
		
	}
	
	
	void m1(int a)
	{
		
	}
	
	
	void m1(int a, int b)
	{
		
	}
	
	
	void m1(float a, float b)
	{
		
	}
	
	void m1(int a, float b)
	{
		
	}
	
	void m1(float a, int b)
	{
		
	}

}
