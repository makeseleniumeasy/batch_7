package MethodsExasmples;

public class MethodUsage {
	
	int i = 10;
	
	int j = i;
	
	
	int k = i+j;
	
	
	int add(int a, int b)
	{/*
		int a1 = a;
		int b1= b;
		
		int c = a1+b1;*/
		
		//System.out.println(c);
		return a+b;
	}
	
	
	public static void main(String[] args) {
		
		
		MethodUsage m = new MethodUsage();
		int c = m.add(10,20);
		System.out.println(c);
		int d= m.add(30, 40);
		System.out.println(d);
		
		
		
	}

}
