package MethodsExasmples;

public class MethodChaining {
	
	void m1()
	{
		System.out.println("M1 starts");
		m2();
		System.out.println("M1 ends");
	}
	
	
	void m2()
	{
		System.out.println("M2 starts");
		m3();
		System.out.println("M2 ends");
	}
	
	
	void m3()
	{
		System.out.println("M3 starts");
		System.out.println("M3 ends");
	}
	
	public static void main(String[] args) {
		
		MethodChaining m = new MethodChaining();
		m.m1();
		//m.m2();
		//m.m3();
	}

}
