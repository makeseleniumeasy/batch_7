package AccessSpecfiers;

public class ProtectedExamples {
	
	protected int i =100;
	
	
	protected ProtectedExamples()
	{
		System.out.println("Super constructor");
	}
	
	public static void main(String[] args) {
		ProtectedExamples p = new ProtectedExamples();
		System.out.println(p.i);
	}

}
