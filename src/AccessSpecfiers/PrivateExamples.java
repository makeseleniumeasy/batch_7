package AccessSpecfiers;

/*private*/ class PrivateExamples {
	
	{
		System.out.println("Non sttaic block");
	}
	
	private int a = 10;
	
	private void disp()
	{
		System.out.println("Disp");
	}
	
	
	private PrivateExamples()
	{
		System.out.println("Object Created");
	}
	
	public static void main(String[] args) {
		
		PrivateExamples pe = new PrivateExamples();
		System.out.println(pe.a);
		pe.disp();
	}

}
