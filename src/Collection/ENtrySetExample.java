package Collection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ENtrySetExample {
	
	
	public static void main(String[] args) {
		
		
		HashMap<String,String> details = new HashMap();
		
		details.put("FN", "ABC");
		details.putIfAbsent("LN", "DEF");
		details.putIfAbsent("LN", "XYZ");
		
		System.out.println(details);
		
		
		Set<Entry<String,String>> entry = details.entrySet();
		
		for(Entry<String,String> e: entry)
		{
			System.out.println(e.getKey());
			System.out.println(e.getValue());
		}
		
		HashMap details1 = new HashMap();
		
		details1.put("FN", "ABC");
		details1.putIfAbsent("LN", "DEF");
		//details.putIfAbsent("LN", "XYZ");
		
		Set<Entry<Object,Object>> e1= details1.entrySet();
		for(Entry<Object,Object> obj: e1)
		{
			System.out.println(obj.getKey());
			System.out.println(obj.getValue());
			
			if(obj.getKey().equals("FN"))
			{
				if(obj.getValue().equals("ABC"))
				{
					details1.remove(obj.getKey());
				}
			}
			
		}
		
		System.out.println(details1);
		
		
		if(details1.get("LN").equals("DEF"))
			details1.remove("LN");
		
		System.out.println(details1);
		
		
		details1.put("FN", "ABC");
		details1.putIfAbsent("LN", "DEF");
		
		details1.remove("FN", "AMOD");
		System.out.println(details1);
		
		details1.put("fn", "SDFDJ");
		System.out.println(details1);
		
		

		
	}

}
