package Collection;

import java.util.ArrayList;

import AbstractClass.AbstractComplete;

public class CollectionINterface {
	
	
	public static void main(String[] args) {
		
		ArrayList a1= new ArrayList<>();
		a1.add(10);     // a1.add(new Integer(10));
		a1.add("Java");
		a1.add(true);
		a1.add('A');
		a1.add(new AbstractComplete());
		
		System.out.println(a1);
		
		
		
		// Adding duplicates
		a1.add(10);
		System.out.println(a1);
		
		a1.add(2, 50);
		
		System.out.println(a1);
		
		// Add All
		ArrayList a2= new ArrayList<>();
		a2.addAll(a1);
		System.out.println(a2);
		
		
		// Insert null
		a1.add(null);
		a1.add("");
		System.out.println(a1);
		
		
		a1.remove("Java");
		System.out.println(a1);
		a1.remove("10");
		System.out.println(a1);
		a1.remove(new Integer(10));
		System.out.println(a1);
		
		a1.remove(1);
		System.out.println(a1);
		
		System.out.println(a2);
		
		//a2.removeAll(a1);
		System.out.println(a2);
		
		System.out.println(a2.size());
		
		System.out.println(a2.indexOf(new Integer(10)));
		
		System.out.println(a2.get(2));
		
		System.out.println(a2.isEmpty());
		
		
		a2.add(4, "B");
		System.out.println(a2);
		a2.set(4, "C");
		System.out.println(a2);
		
		
		ArrayList a3= new ArrayList();
		a3.add(10);
		a3.add(40);
		a3.add(50);

		ArrayList a4= new ArrayList();
		a4.add(10);
		a4.add(40);
		a4.add(30);
		System.out.println(a3.containsAll(a4));
		
		System.out.println(a4.contains(10));
		
		a4.retainAll(a3);
		System.out.println(a4);
		System.out.println(a3);
		
		
		Object[] inArray= a4.toArray();
		System.out.println(inArray.length);
		
	}

}
