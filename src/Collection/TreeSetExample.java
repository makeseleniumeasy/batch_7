package Collection;

import java.util.TreeSet;

public class TreeSetExample {
	
	
	public static void main(String[] args) {
		
		
		TreeSet ts = new TreeSet<>();
		ts.add("Java");
		ts.add("10");
		ts.add("ABC");
		ts.add("20");
	    ts.add("_DOG");
	    ts.add("10");
		
		System.out.println(ts);
	}

}
