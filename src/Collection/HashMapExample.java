package Collection;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class HashMapExample {
	
	
	public static void main(String[] args) {
		
		
		Map hm = new HashMap();
		hm.put("FirstName", "Amod");
		hm.put("LastName","MAhajan");
		
		System.out.println(hm);
		
		
		//System.out.println(hm.get("LastName"));
		
		//System.out.println(hm.containsKey("LastName1"));
		
		hm.put("LastName", "ABC");
		
		
		System.out.println(hm);
		System.out.println(hm.get("LastName"));
		
		hm.put("Address","Amod");
		
		System.out.println(hm);
		
		Set allKeys= hm.keySet();
		for(Object key:allKeys)
			System.out.println(key);
		
		Collection allValues=  hm.values();
		for(Object val : allValues)
			System.out.println(val);
		
		Set s= hm.entrySet();
		for(Object o: s)
		{
			System.out.println(o);
		}
		
		
		
	}
	

}
