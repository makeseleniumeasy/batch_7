package Collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import AbstractClass.AbstractComplete;

public class IteratingArrayList {
	
	
	public static void main(String[] args) {
		

		ArrayList a1= new ArrayList<>();
		a1.add(10);     // a1.add(new Integer(10));
		a1.add("Java");
		a1.add(true);
		a1.add('A');
		a1.add(new AbstractComplete());
		
		System.out.println(a1);
		
		
		/*for(int i =0 ; i< a1.size() ; i++)
		{
			System.out.println(a1.get(i));
		}*/
		
		/*
		for(Object Obj : a1)
		{
			System.out.println(Obj);
		}*/
		
		
		/*Iterator i1= a1.iterator();
		while(i1.hasNext())
		{
			if(i1.next().equals(new Integer(10)))
				i1.remove();
		}
		System.out.println(a1)*/;
		
		
		//ListIterator l1= a1.listIterator();
		ListIterator l1= a1.listIterator(a1.size());
		/*while(l1.hasNext())
		{
			System.out.println(l1.next());
		}*/
		
		while(l1.hasPrevious())
		{
			System.out.println(l1.previous());
		}
		
		
		
		
	}

}
