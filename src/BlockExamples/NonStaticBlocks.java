package BlockExamples;

public class NonStaticBlocks {
	
	
	static int i;
	
	{
		i = 20;
		System.out.println("I is :"+i);
		System.out.println("Non static Block 1");
		disp();
	}
	
	
	{
		System.out.println("Non static Block 2");
	}
	
	NonStaticBlocks()
	{
		System.out.println("COnstructor");
	}
	
	
	
	static void disp()
	{
		System.out.println("ABC DIPS");
	}
	
	public static void main(String[] args) {
		
		NonStaticBlocks obj = new NonStaticBlocks();
	}

}
