package BlockExamples;


import Pkg1.*;
import Pkg1.Pkg2.*;
import Pkg1.Pkg2.Pkg3.*;

public class AccessPackage {
	
	public static void main(String[] args) {
		
		Pkg1Class p1 = new Pkg1Class();
		Pkg2Class p2 = new Pkg2Class();
		Pkg3Class p3 = new Pkg3Class();
		
		
		Pkg1.A a1 = new Pkg1.A();
		Pkg1.Pkg2.Pkg3.A a2 = new Pkg1.Pkg2.Pkg3.A();
		Pkg1.Pkg2.A a3 = new Pkg1.Pkg2.A();
		
	}
	

}
