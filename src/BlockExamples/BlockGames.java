package BlockExamples;


class SuperBlock
{
	{
		System.out.println("Non- Static block");
	}
	
	static {
		System.out.println("Static block");
	}
}
public class BlockGames extends SuperBlock{
	
	{
		System.out.println("Non- Static block Child");
	}
	
	static {
		System.out.println("Static block child");
	}
	
	
	public static void main(String[] args) {
		BlockGames b = new BlockGames();
		
	}

}
