package StaticNonStatic;

public class NonStaticExampole {
	
	
	int i = 10;
	
	
	static int j =30;
	
	void disp()
	{
		System.out.println("Disp");
	}
	
	
	public static void main(String[] args) {
		
		NonStaticExampole n = new NonStaticExampole();
		//n.disp();
		//System.out.println(n.i);
		System.out.println(NonStaticExampole.j);
		//n.i= 20;
		//System.out.println(n.i);
		NonStaticExampole.j=100;
		System.out.println(NonStaticExampole.j);
		
		
		NonStaticExampole n1 = new NonStaticExampole();
		//n1.disp();
		//System.out.println(n1.i);
		System.out.println(NonStaticExampole.j);
		
	}

}
