package StaticNonStatic;

public class NonStaticExample1 {
	
	void disp()
	{
		System.out.println("Disp");
	}
	
	static void show()
	{
		System.out.println("Show");
	}
	
	public static void main(String[] args) {
		//NonStaticExample1 n= new NonStaticExample1();
		//n.disp();
		//n.show();
		NonStaticExample1.show();
		
		
	}

}
