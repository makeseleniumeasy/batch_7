package StaticNonStatic;

public class StaticNonStaticGame {
	
	static int i =20;
	
	static void show()
	{
		System.out.println("Show");
		StaticNonStaticGame.disp();
		
		System.out.println(i);
		
		//StaticNonStaticGame s = new StaticNonStaticGame();
		//s.print();
	}
	
	
	static void disp()
	{
		System.out.println("Disp");
	}
	
	
	void print()
	{
		System.out.println("print");
		disp();
	}
	
	
	public static void main(String[] args) {
		StaticNonStaticGame.show();
		
	}

}
