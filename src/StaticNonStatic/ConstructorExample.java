package StaticNonStatic;

public class ConstructorExample {

	ConstructorExample()
	{
		disp();
		show();
	}
	
	
	void disp()
	{
		System.out.println("Disp");
	}
	
	
	static void show()
	{
		System.out.println("show");
	}
	
	public static void main(String[] args) {
		
		ConstructorExample c = new ConstructorExample();
	}
}
