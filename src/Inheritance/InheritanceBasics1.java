package Inheritance;



class SuperClass1
{
	private void disp()
	{
		System.out.println("Super Class Method");
	}
	
	public void show()
	{
		System.out.println("Super Class Method");
	}
}

public class InheritanceBasics1 extends SuperClass1{


	
	public static void main(String[] args) {
		InheritanceBasics1 i = new InheritanceBasics1();
		//i.disp();
		i.show();
		
	}

}
