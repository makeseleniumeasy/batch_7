package Inheritance;



class SuperClass
{
	public void disp()
	{
		System.out.println("Super Class Method");
	}
	
	public void show()
	{
		System.out.println("Super Class Method");
	}
}

public class InheritanceBasics extends SuperClass{


	
	public static void main(String[] args) {
		InheritanceBasics i = new InheritanceBasics();
		i.disp();
		i.show();
		
	}

}
