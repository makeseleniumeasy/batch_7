package Inheritance;



class SuperClass2
{
	final void disp()
	{
		System.out.println("Super Class disp Method");
	}
	
	
	static void print()
	{
		System.out.println("Super class Print method");
	}
	
	public void show()
	{
		System.out.println("Super Class show Method");
	}
}

public class InheritanceBasics2 extends SuperClass2{


	
	public static void main(String[] args) {
		InheritanceBasics2 i = new InheritanceBasics2();
		i.disp();
		i.show();
		i.print();
		
	}

}
