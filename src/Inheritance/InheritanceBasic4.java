package Inheritance;


class Super
{
	void body()
	{
		System.out.println("Super class body");
	}
	
	
	void show()
	{
		System.out.println("Show");
	}
}

public class InheritanceBasic4 extends Super
{

	void body()
	{
		System.out.println("Sub class body");
	}
	
	
	void print()
	{
		System.out.println("Print");
	}
	
	
	public static void main(String[] args) {
	/*	
		InheritanceBasic4 i = new InheritanceBasic4();
		i.body();
		
		Super s = new Super();
		s.body();*/
		
		Super s1= new InheritanceBasic4();
		s1.body();
		s1.show();
		
		InheritanceBasic4 i2 = (InheritanceBasic4)s1;
		i2.print();
	}
}
