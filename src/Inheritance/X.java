package Inheritance;

class Y
{
	static void disp()
	{
		System.out.println("Disp 2");
	}
	
	 void print()
	{
		System.out.println("print2");
	}
}

public class X extends Y{
	
	
	static void disp()
	{
		System.out.println("Disp 1");
	}
	
	@Override
	void print()
	{
		System.out.println("print1");
	}
	
	public static void main(String[] args) {
		
		
	/*	X x = new X();
		x.disp();*/
		
		Y y = new X();
		y.disp();
		y.print();
		
		
	}

}
