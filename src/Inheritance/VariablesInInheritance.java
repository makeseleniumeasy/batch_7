package Inheritance;



class VaarClass
{
	int i;
	/*public static void main(String[] args) {
		//i = 40;
		
	}*/
}

public class VariablesInInheritance extends VaarClass{
	
	int i =200;
	
	void print()
	{
		System.out.println(i);
		i = 100;
		System.out.println(i);
		System.out.println(super.i);
	}
	
	public static void main(String[] args) {
		
		VariablesInInheritance obj = new VariablesInInheritance();
		//obj.i =50;
		//System.out.println(obj.i);
		obj.print();
	}

}
