package Inheritance;


class A
{
	void disp()
	{
		System.out.println("A");
	}
	
	void show()
	{
		System.out.println("Show");
	}
}


class B extends A
{
	void disp()
	{
		System.out.println("B");
	}
	
	void print()
	{
		System.out.println("Print");
	}
}

public class C extends B
{
	/*void disp()
	{
		System.out.println("C");
	}*/
	
	
	public static void main(String[] args) {
		
		/*C c = new C();
		c.disp();
		
		A a = new C();
		c.disp();
		
		B b = (B)a;
		b.disp();*/
		
		
		A a = new A();
		a.show();
		a.disp();
		
		A a1 = new C();
		a1.disp();
		a1.show();
		B b = (B)a1;
		b.print();
	}
}
