package Inheritance;



class VaarClass1
{
	static int i;
	/*public static void main(String[] args) {
		//i = 40;
		
	}*/
}

public class StaticVariablesInInheritance2 extends VaarClass1{
	
	int i =200;
	
	void print()
	{
		System.out.println(i);
		i = 100;
		System.out.println(i);
		System.out.println(super.i);
	}
	
	public static void main(String[] args) {
		
		StaticVariablesInInheritance2 obj = new StaticVariablesInInheritance2();
		//obj.i =50;
		//System.out.println(obj.i);
		obj.print();
		
		System.out.println(obj.i);
	}

}
