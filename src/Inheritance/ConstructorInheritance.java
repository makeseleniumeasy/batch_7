package Inheritance;


class Top1
{
	Top1()
	{
		System.out.println("Top class constructor");
	}
}
public class ConstructorInheritance extends Top1{

	
	ConstructorInheritance()
	{
		System.out.println("SUb class constructor");
	}
	
	
	public static void main(String[] args) {
		ConstructorInheritance c = new ConstructorInheritance();
		
	}
}
