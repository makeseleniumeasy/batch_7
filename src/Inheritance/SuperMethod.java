package Inheritance;

class Top
{
	void disp()
	{
		System.out.println("I am Top class Disp.");
	}
}

public class SuperMethod extends Top{

	void disp()
	{
		super.disp();
		System.out.println("I am Sub class Disp.");
	}
	
	
	public static void main(String[] args) {
		
		
		SuperMethod m = new SuperMethod();
		m.disp();
	}
	
}
