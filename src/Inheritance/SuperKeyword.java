package Inheritance;


class Top4
{
	 void disp()
	{
		System.out.println("Super Disp");
	}
}

public class SuperKeyword extends Top4{
	
	
	/*void disp()
	{
		super.disp();
		System.out.println("Super Disp");
	}*/
	
	 void show()
	{
		super.disp();
		System.out.println("Super disp");
	}

	public static void main(String[] args) {
		
		//super.disp();
	}
}
