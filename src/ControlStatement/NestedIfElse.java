package ControlStatement;

public class NestedIfElse {
	
	public static void main(String[] args) {
		
		System.out.println("Program starts....");
		int shoppingAmount = 10000;
		int discount=0;
		
		if(shoppingAmount > 3000)
		{
			discount = 10;
		}
		else if(shoppingAmount > 2000)
		{
			discount = 8;
		}
		else
			discount=0;
		
		
		System.out.println(discount);
	}

}
