package ControlStatement;

public class WhileExample {
	
	public static void main(String[] args) {
		
		int i =0;
		
		/*while(i<= 10)
		{
			System.out.println("Java");
			i = i+2;
		}*/
		
		int shopping = 10000;
		//String gender= "male";
		int count = 2;
		
		do
		{
			//System.out.println("AMod");
			//i++;
			
			shopping = shopping - (shopping*10/100);
			System.out.println(shopping);
			count--;
		}while(count >0);
	}

}
