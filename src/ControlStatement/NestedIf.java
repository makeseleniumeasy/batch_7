package ControlStatement;

public class NestedIf {
	
	public static void main(String[] args) {
		
		int shoppingAmount = 10000;
		String gender = "male";
		
		if(shoppingAmount > 5000)
		{
			if(gender.equals("male"))
				System.out.println("Discount is 10.");
			else
				System.out.println("Discount is 15.");
		}
	}

}
