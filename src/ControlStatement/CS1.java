package ControlStatement;

public class CS1 {
	
	public static void main(String[] args) {
		
		//System.out.println("I am a teacher.");
		//System.out.println("I am a student");
		

		/*
		 * COntrol staments:
		 * 1. if
		 * 2. if..else
		 * 3. Nested if else
		 * 4. Switch		 * 
		 */
		
		int marks = 49;
		
		if(marks>=50)
		{
			System.out.println("You are pass");
		}
		else
		{
			System.out.println("You are failed.");
		}
			
		
		System.out.println((marks>=50) ? "You are pass" : "Fail");
		
		
		//10>20 ? "ABC" : (5>6 ? 5 :6)
			
				
		
	}

}
