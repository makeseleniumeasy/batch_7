package FinalExamples;

class FinalSuper
{
	final void dips()
	{
		System.out.println("Disp disp");
	}
}

public class FinalMethod extends FinalSuper{
	
	
	final void dips()
	{
		System.out.println("Disp disp");
	}
	
	public static void main(String[] args) {
		
		FinalMethod f = new FinalMethod();
		f.dips();
	}

}
