package Operators;

public class PrintStatements {
	
	public static void main(String[] args) {
		
		String s1 = "Selenium";
		System.out.println(s1);
		System.out.println("Selenium");
		
		System.out.println("Selenium"+s1);
		
		System.out.println("Value of S1 is :"+s1);
		
		System.out.println("Value of \"S1\" is :"+s1);
		
		System.out.println("10+20");
		
		System.out.println(10+20);
		
		int i1= 20;
		int i2= 30;
		
		System.out.println(i1+i2);
		
		//boolean TRUE = false;
		
		System.out.println(true);
		
		System.out.println(true & false);
		System.out.println(true | false);
		
	}

}
