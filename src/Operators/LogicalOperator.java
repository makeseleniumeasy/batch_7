package Operators;

public class LogicalOperator {
	
	public static void main(String[] args) {
		
		/*System.out.println(12 < 2);
		System.out.println(12 > 2);
		
		
		System.out.println(!(12 > 2));*/
		
		System.out.println(true && true);
		System.out.println(true & true);
		
		System.out.println(true || true);
		System.out.println(true |  true);
		
		
		System.out.println(10>2 && 67< 4 && 56>10); //(==> 3 conditions)
		System.out.println(10>2 & 67< 4 & 56>10); // 2 cond
		
		//System.out.println("sel" > "dff");
		System.out.println('c' > 'a');
	}

}
