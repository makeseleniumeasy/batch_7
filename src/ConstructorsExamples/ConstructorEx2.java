package ConstructorsExamples;

public class ConstructorEx2 {
	
	
	int k;
	int m;
	
	
	ConstructorEx2()
	{
		System.out.println("No arg constructor");
		k =0;
		m=0;
		System.out.println(k);
		System.out.println(m);
	}
	
	
	
	ConstructorEx2(int a, int b)
	{
		System.out.println("2 arg constructor");
		//k =a;
		//m=b;
		System.out.println(a);
		System.out.println(b);
	}
	
	ConstructorEx2(int a)
	{
		System.out.println("1 arg constructor");
		k=m=a;
		System.out.println(k);
		System.out.println(m);
	}
	
	
	public static void main(String[] args) {
		
		ConstructorEx2 c1= new ConstructorEx2();
		ConstructorEx2 c2= new ConstructorEx2(10,20);
		ConstructorEx2 c3= new ConstructorEx2(30);
	}
	

}
