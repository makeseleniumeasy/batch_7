package Interface;


interface I11
{
	void add();
}


interface I22
{
	void add();
}

public  class MultipleInheritanceInterface implements I11,I22{

	@Override
	public void add() {
		System.out.println("Add");
		
	}
	
	
	public static void main(String[] args) {
		
		MultipleInheritanceInterface m = new MultipleInheritanceInterface();
		m.add();
		
		
		I11 i1= new  MultipleInheritanceInterface();
		i1.add();
		
	}

}
