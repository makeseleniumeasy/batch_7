package Arrays;

import java.util.Scanner;

public class ArrayIteration {
	
	public static void main(String[] args) {
		
		int marks[] = new int[5];
		
		Scanner sc = new Scanner(System.in);
		
		for(int i =0 ; i < marks.length ;i++)
		{
			System.out.println("Please enter marks for subject : "+(i+1));
			marks[i]= sc.nextInt();
		}
		
		// print values of array
		
		for(int i =0 ; i < marks.length ;i++)
		{
			System.out.println("Marks of subject : "+(i+1)+":"+marks[i]);
		}
	}

}
