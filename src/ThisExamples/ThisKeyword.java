package ThisExamples;

public class ThisKeyword {

	
	int a;
	int b;
	
	
	ThisKeyword(int a, int b) {
		this.a=a;
		this.b=b;
		System.out.println(a);
		System.out.println(b);
	}
	
	
	
	public static void main(String[] args) {
		
		ThisKeyword t = new ThisKeyword(10, 20);
		
		System.out.println(t.a);
		System.out.println(t.b);
		System.out.println("========================");
		
		
		ThisKeyword t1 = new ThisKeyword(100, 200);
		
		System.out.println(t1.a);
		System.out.println(t1.b);
		System.out.println("========================");
	}
}
